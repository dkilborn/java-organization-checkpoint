package com.galvanize.formattersTest;

import com.galvanize.Booking;
import com.galvanize.BookingCodeInvalidException;
import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FormattersTest {
    @Test
    public void CSVFormatterShouldReturnAFormattedString() throws BookingCodeInvalidException {
        //-----SETUP-----
        CSVFormatter test = new CSVFormatter();
        Booking myBooking = new Booking("a111-08:30am-11:00am");

        //-----ENACT-----
        String result = test.format(myBooking);
        System.out.println(result);

        //-----ASSERT-----
        assertEquals("type,room number,start time,end time\n" +
                "Auditorium,111,08:30am,11:00am", result);

        //-----TEARDOWN-----
    }

    @Test
    public void JSONFormatterShouldReturnAFormattedString() throws BookingCodeInvalidException {
        //-----SETUP-----
        JSONFormatter test = new JSONFormatter();
        Booking myBooking = new Booking("s111-08:30am-11:00am");

        //-----ENACT-----
        String result = test.format(myBooking);

        //-----ASSERT-----
        assertEquals("{\n" +
                "  \"type\": \"Suite\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}",result);
        //-----TEARDOWN-----
    }
    @Test
    public void HTMLFormatterShouldReturnAFormattedString() throws BookingCodeInvalidException {
        //-----SETUP-----
        HTMLFormatter test = new HTMLFormatter();
        Booking myBooking = new Booking("r111-08:30am-11:00am");

        //-----ENACT-----
        String result = test.format(myBooking);

        //-----ASSERT-----
        assertEquals("<dl>\n" +
                "  <dt>Type</dt><dd>Conference Room</dd>\n" +
                "  <dt>Room Number</dt><dd>111</dd>\n" +
                "  <dt>Start Time</dt><dd>08:30am</dd>\n" +
                "  <dt>End Time</dt><dd>11:00am</dd>\n" +
                "</dl>",result);

        //-----TEARDOWN-----
    }

    @Test
    public void CSVFormatterShouldReturnAFormattedStringRoomTypeClassRoom() throws BookingCodeInvalidException {
        //-----SETUP-----
        CSVFormatter test = new CSVFormatter();
        Booking myBooking = new Booking("c111-08:30am-11:00am");

        //-----ENACT-----
        String result = test.format(myBooking);
        System.out.println(result);

        //-----ASSERT-----
        assertEquals("type,room number,start time,end time\n" +
                "Classroom,111,08:30am,11:00am", result);

        //-----TEARDOWN-----
    }
}
