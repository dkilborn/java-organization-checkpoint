package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class BookingTest {
    //WRITE SOME BOOKING TESTS AND EXCEPTION TESTS
    @Test
    public void parseShouldThrowInvalidBookingCodeExceptionIfInputIsInvalid() throws BookingCodeInvalidException {
        //-----SETUP-----


        //-----ENACT-----

        //-----ASSERT-----
        assertThrows(BookingCodeInvalidException.class, () -> Booking.parse("b123-08:00am-09:30am"));

        //-----TEARDOWN-----
    }



}
