package com.galvanize;

import com.sun.tools.javac.Main;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.Format;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void getFormattersThrowsInvalidFormatException() {
        // Write your tests here
        // outContent.toString() will give you what your code printed to System.out
        //SETUP
        String[] args = new String[] {"a111-08:00am-09:00am","fakeFormat"};

        //ENACT

        //ASSERT
        FormatTypeInvalidException e = assertThrows(FormatTypeInvalidException.class, () -> Application.main(args));
        assertEquals("fakeFormat is not a valid format type. Please select JSON, CSV, or HTML.", e.getMessage());
    }

    @Test
    public void getFormatterShouldReturnCorrectFormatterTypes() throws FormatTypeInvalidException {
        //-----SETUP-----
        String[] args1 = new String[] {"doesn't matter","html"};
        String[] args2 = new String[] {"doesn't matter","csv"};
        String[] args3 = new String[] {"doesn't matter","json"};

        //-----ENACT-----
        String result1 = Application.getFormatter(args1[1]).getClass().toString();
        String result2 = Application.getFormatter(args2[1]).getClass().toString();
        String result3 = Application.getFormatter(args3[1]).getClass().toString();
        //-----ASSERT-----
        assertEquals("class com.galvanize.formatters.HTMLFormatter",result1);
        assertEquals("class com.galvanize.formatters.CSVFormatter",result2);
        assertEquals("class com.galvanize.formatters.JSONFormatter",result3);

        //-----TEARDOWN-----
    }
    @Test
    public void applicationShouldOutputTheParsedBookingCodeWithTheCorrectFormat() throws FormatTypeInvalidException, BookingCodeInvalidException {
        //-----SETUP-----
        Application myApplication = new Application();
        String[] args = new String[] {"a111-08:30am-11:00am", "csv"};

        //-----ENACT-----
        Application.main(args);

        //-----ASSERT-----
        assertEquals("type,room number,start time,end time\n" +
                "Auditorium,111,08:30am,11:00am\n",outContent.toString());

        //-----TEARDOWN-----
    }

}