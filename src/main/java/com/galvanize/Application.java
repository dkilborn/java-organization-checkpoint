package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;

public class Application {
    public static void main(String[] args) throws BookingCodeInvalidException, FormatTypeInvalidException {

        Formatter formatType = getFormatter(args[1]);

        //Booking myBooking = new Booking(args[0]);

        System.out.println(formatType.format(Booking.parse(args[0])));

    }

    public static Formatter getFormatter(String formatType) throws FormatTypeInvalidException {
        switch (formatType){
            case "json":{
                return new JSONFormatter();
            }
            case "csv":{
                return new CSVFormatter();
            }
            case "html":{
                return new HTMLFormatter();
            }
            default:
                throw new FormatTypeInvalidException(String.format("%s is not a valid format type. Please select JSON, CSV, or HTML.",formatType));
        }
    }
}