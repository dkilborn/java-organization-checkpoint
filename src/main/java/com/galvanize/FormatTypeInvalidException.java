package com.galvanize;

public class FormatTypeInvalidException extends Exception {
    public FormatTypeInvalidException(String format) {
        super(format);
    }
}
