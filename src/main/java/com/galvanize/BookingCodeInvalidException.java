package com.galvanize;

public class BookingCodeInvalidException extends Exception {
    public BookingCodeInvalidException(String s) {
        super(s);
    }
}
