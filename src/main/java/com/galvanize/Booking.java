package com.galvanize;


public class Booking {

    enum RoomType{
        CONFERENCE_ROOM{
            @Override
            public String toString() {
                return "Conference Room";
            }
        },
        SUITE{
            @Override
            public String toString(){
                return "Suite";
            }
        },
        AUDITORIUM{
            @Override
            public String toString() {
                return "Auditorium";
            }
        },
        CLASSROOM{
            @Override
            public String toString(){
                return "Classroom";
            }
        }
    }

    final private String roomNumber;
    final private String startTime;
    final private String endTime;
    final private RoomType roomType;

    static Booking parse(String bookingCode) throws BookingCodeInvalidException {
        String roomTypeCode = bookingCode.substring(0,1);
        bookingCode = bookingCode.substring(1);
        String[] otherInfo = bookingCode.split("-");
        String roomNumber = otherInfo[0];
        String startTime = otherInfo[1];
        String endTime = otherInfo[2];
        RoomType parsed;
        switch (roomTypeCode) {
            case "a": {
                parsed = RoomType.AUDITORIUM;
                break;
            }
            case "c": {
                parsed = RoomType.CLASSROOM;
                break;
            }
            case "r": {
                parsed = RoomType.CONFERENCE_ROOM;
                break;
            }
            case "s": {
                parsed = RoomType.SUITE;
                break;
            }
            default:
                System.out.println("Something went wrong.");
                throw new BookingCodeInvalidException("Please enter a valid booking code.");
        }

        return new Booking(parsed,roomNumber,startTime,endTime);
    }

    public Booking(String bookingCode) throws BookingCodeInvalidException {
        Booking booking = parse(bookingCode);
        this.roomNumber = booking.getRoomNumber();
        this.endTime = booking.getEndTime();
        this.startTime = booking.getStartTime();
        this.roomType = booking.getRoomType();
    }

    public Booking(RoomType parsed, String roomNumber, String startTime, String endTime) {
        this.roomType = parsed;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public RoomType getRoomType() {
        return roomType;
    }
}
