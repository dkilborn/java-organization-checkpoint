package com.galvanize.formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter{
    @Override
    public String format(Booking input) {
        return
                "type,room number,start time,end time\n" +
                input.getRoomType()+","+input.getRoomNumber()+","+input.getStartTime()+","+input.getEndTime();
    }
}
