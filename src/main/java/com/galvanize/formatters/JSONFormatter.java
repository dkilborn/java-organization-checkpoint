package com.galvanize.formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter{
    @Override
    public String format(Booking input) {
        return
                "{\n" +
                    "  \"type\": \"" + input.getRoomType() + "\",\n" +
                    "  \"roomNumber\": " + input.getRoomNumber() + ",\n" +
                    "  \"startTime\": \"" + input.getStartTime() + "\",\n" +
                    "  \"endTime\": \"" + input.getEndTime() + "\"\n" +
                "}";
    }
}
