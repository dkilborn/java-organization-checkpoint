package com.galvanize.formatters;

import com.galvanize.Booking;

public class HTMLFormatter implements Formatter{
    @Override
    public String format(Booking input) {
        return
                "<dl>\n" +
                "  <dt>Type</dt><dd>" + input.getRoomType()+"</dd>\n" +
                "  <dt>Room Number</dt><dd>" + input.getRoomNumber() + "</dd>\n" +
                "  <dt>Start Time</dt><dd>" + input.getStartTime() + "</dd>\n" +
                "  <dt>End Time</dt><dd>" + input.getEndTime() + "</dd>\n" +
                "</dl>";
    }
}
